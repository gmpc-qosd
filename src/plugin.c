/* gmpc-qosd (GMPC plugin)
 * Copyright (C) 2006-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <gmpc/plugin.h>
#include <config.h>
#define QOSD_SONG_MARKUP "[[<span size=\"large\">♪ %title% ♪</span>\n]<span size=\"x-small\">[<i>artist:</i> %artist% ][<i>album:</i> %album%]</span>]|[%name%]|[%shortfile%]"


GtkWidget *qosd_vbox = NULL;

void qosd_mpd_status_changed(MpdObj *mi, ChangedStatusType what, void *data);
void qosd_construct(GtkWidget *container);
void qosd_destroy(GtkWidget *container);
static int qosd_get_enabled(void);
static void qosd_set_enabled(int enabled);

gmpcPrefPlugin qosd_gpp = {
	qosd_construct,
	qosd_destroy
};

int plugin_api_version = PLUGIN_API_VERSION;
/* main plugin_osd info */
gmpcPlugin plugin = {
	.name           = "Qball's On Screen Display",
	.version        = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
	.plugin_type    = GMPC_PLUGIN_NO_GUI,
	.mpd_status_changed = &qosd_mpd_status_changed,
	.pref           = &qosd_gpp,
	.get_enabled    = qosd_get_enabled,
	.set_enabled    = qosd_set_enabled	
};


static int qosd_get_enabled(void)
{
	return cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "enable", TRUE);
}
static void qosd_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, "qosd-plugin", "enable", enabled);
}

static void qosd_song_changed(MpdObj *mi)
{
	gchar buffer[1024];
	int state = 0;
	mpd_Song *song = NULL;
	if(!cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "enable", 0))
	{
		return;
	}
	state = mpd_player_get_state(connection);
	song = mpd_playlist_get_current_song(connection);
	if(song && (state == MPD_STATUS_STATE_PLAY || state == MPD_STATUS_STATE_PAUSE) )
	{
		/* this is stupid and inefficient, but it's late and I am to lame to fix it */
		int i= 0, j = 0;
		char buffer2[1024];
		char *markup = cfg_get_single_value_as_string_with_default(config, "qosd-plugin","markup",QOSD_SONG_MARKUP);
		memset(buffer,'\0',1024);
		mpd_song_markup_escaped(buffer,1024,
				markup,
			       	song);
		cfg_free_string(markup);
        qosd_show_popup(buffer);
	}
	else
	{
        qosd_show_popup("Stopped");
    }
}

static void qosd_spin_value_changed(GtkWidget *wid)
{
	int kk = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(wid));
	cfg_set_single_value_as_int(config, "qosd-plugin", "timeout", kk);
}


static void qosd_x_offset_changed(GtkWidget *wid)
{
	int kk = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(wid));
	cfg_set_single_value_as_int(config, "qosd-plugin", "x-offset", kk);
}
static void qosd_x_expand_changed(GtkWidget *wid)
{
	int kk = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(wid));
	cfg_set_single_value_as_int(config, "qosd-plugin", "expand", kk);
}



static void qosd_y_offset_changed(GtkWidget *wid)
{
	int kk = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(wid));
	cfg_set_single_value_as_int(config, "qosd-plugin", "y-offset", kk);
}
static void qosd_test_settings()
{
	qosd_show_popup("Test string:\nGnome Music Player Client");
}

static void qosd_color_set(GtkColorButton *colorbut)
{
	GdkColor color;
	gchar *string = NULL;
	gtk_color_button_get_color(colorbut, &color);
	cfg_set_single_value_as_int(config, "qosd-plugin", "text-rcolor", color.red);
	cfg_set_single_value_as_int(config, "qosd-plugin", "text-gcolor", color.green);
	cfg_set_single_value_as_int(config, "qosd-plugin", "text-bcolor", color.blue);
	cfg_set_single_value_as_int(config, "qosd-plugin", "text-acolor", gtk_color_button_get_alpha(colorbut));
	
}

void qosd_destroy(GtkWidget *container)
{
	gtk_container_remove(GTK_CONTAINER(container), qosd_vbox);
}

static void qosd_bgcolor_set(GtkColorButton *colorbut)
{
	GdkColor color;
	gchar *string = NULL;
	gtk_color_button_get_color(colorbut, &color);
	cfg_set_single_value_as_int(config, "qosd-plugin", "background-rcolor", color.red);
	cfg_set_single_value_as_int(config, "qosd-plugin", "background-gcolor", color.green);
	cfg_set_single_value_as_int(config, "qosd-plugin", "background-bcolor", color.blue);
	cfg_set_single_value_as_int(config, "qosd-plugin", "background-acolor", gtk_color_button_get_alpha(colorbut));
	
}

static void qosd_vert_pos_changed(GtkComboBox *box)
{
	int i = gtk_combo_box_get_active(box);
	cfg_set_single_value_as_int(config, "qosd-plugin", "vposition", i);
}
static void qosd_horz_pos_changed(GtkComboBox *box)
{
	int i = gtk_combo_box_get_active(box);
	cfg_set_single_value_as_int(config, "qosd-plugin", "hposition", i);
}


void qosd_construct(GtkWidget *container)
{
	GtkWidget *label = NULL;
	GtkWidget *wid2 = NULL;
	GtkWidget *table = NULL;
	gchar *string = NULL;
	GdkColor color;
	qosd_vbox = gtk_vbox_new(FALSE,6);
	/* table */
	table = gtk_table_new(8,2,FALSE);


	/* enable/disable */

	gtk_table_set_col_spacings(GTK_TABLE(table), 6);
	gtk_table_set_row_spacings(GTK_TABLE(table), 6);

	gtk_box_pack_start(GTK_BOX(qosd_vbox), table, FALSE, FALSE,0);

	/* timeout */
	label = gtk_label_new("Timeout:"); 
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,0,1,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);	
	label = gtk_spin_button_new_with_range(1.0,10.0,1.0); 
	gtk_table_attach_defaults(GTK_TABLE(table), label, 1,2,0,1);	
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(label),(gdouble)cfg_get_single_value_as_int_with_default(config, "qosd-plugin","timeout",3));
	g_signal_connect(G_OBJECT(label), "value-changed", G_CALLBACK(qosd_spin_value_changed), NULL);

	/* Color Selector */
	label = gtk_label_new("Text Color:");        
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,1,2,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);	
	color.red = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-rcolor", 65535);
	color.green = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-gcolor",65535);
	color.blue = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-bcolor", 65535);
	cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-acolor", 65535);

	label = gtk_color_button_new_with_color(&color);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 1,2,1,2);	
	g_signal_connect(G_OBJECT(label), "color-set", G_CALLBACK(qosd_color_set), NULL);
	gtk_color_button_set_use_alpha (GTK_COLOR_BUTTON(label), TRUE);
	gtk_color_button_set_alpha(GTK_COLOR_BUTTON(label),
			cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-acolor", 65535));



	/* Color Selector */
	label = gtk_label_new("Background Color:");        
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,2,3,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);	
	color.red = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-rcolor", 0);
	color.green = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-gcolor",0);
	color.blue = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-bcolor", 0);
	cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-acolor", 35535);

	label = gtk_color_button_new_with_color(&color);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 1,2,2,3);	
	g_signal_connect(G_OBJECT(label), "color-set", G_CALLBACK(qosd_bgcolor_set), NULL);
	gtk_color_button_set_use_alpha (GTK_COLOR_BUTTON(label), TRUE);
	gtk_color_button_set_alpha(GTK_COLOR_BUTTON(label),
			cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-acolor", 35535));



	
	/* position selector */
	/* TODO*/
	label = gtk_label_new("Vertical Position:"); 
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,3,4,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);		
	label = gtk_combo_box_new_text();
	gtk_combo_box_append_text(GTK_COMBO_BOX(label),"Top");
	gtk_combo_box_append_text(GTK_COMBO_BOX(label),"Middle");
	gtk_combo_box_append_text(GTK_COMBO_BOX(label),"Bottom");

	gtk_combo_box_set_active(GTK_COMBO_BOX(label), cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "vposition", 1));
	g_signal_connect(label, "changed", G_CALLBACK(qosd_vert_pos_changed), NULL);
	gtk_table_attach(GTK_TABLE(table), label, 1,2,3,4,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);		


	
	/* x-offset */
	label = gtk_label_new("Vertical-Offset:"); 
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,4,5,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);	
	label = gtk_spin_button_new_with_range(0.0,100.0,1.0); 
	gtk_table_attach_defaults(GTK_TABLE(table), label, 1,2,4,5);	
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(label),(gdouble)cfg_get_single_value_as_int_with_default(config, "qosd-plugin","x-offset",0));
	g_signal_connect(G_OBJECT(label), "value-changed", G_CALLBACK(qosd_x_offset_changed), NULL);


	/* position selector */
	/* TODO*/
	label = gtk_label_new("Horizontal Position:"); 
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,5,6,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);		
	label = gtk_combo_box_new_text();                                                                                              	
	gtk_combo_box_append_text(GTK_COMBO_BOX(label),"Left");
	gtk_combo_box_append_text(GTK_COMBO_BOX(label),"Center");
	gtk_combo_box_append_text(GTK_COMBO_BOX(label),"Right");
	gtk_combo_box_set_active(GTK_COMBO_BOX(label), cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "hposition", 0));
	g_signal_connect(label, "changed", G_CALLBACK(qosd_horz_pos_changed), NULL);
	gtk_table_attach(GTK_TABLE(table), label, 1,2,5,6,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);		




	/* y-offset */
	label = gtk_label_new("Horizontal-Offset:"); 
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,6,7,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);	
	label = gtk_spin_button_new_with_range(0.0,100.0,1.0); 
	gtk_table_attach_defaults(GTK_TABLE(table), label, 1,2,6,7);	
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(label),(gdouble)cfg_get_single_value_as_int_with_default(config, "qosd-plugin","y-offset",0));
	g_signal_connect(G_OBJECT(label), "value-changed", G_CALLBACK(qosd_y_offset_changed), NULL);



	/* y-offset */
	label = gtk_label_new("Horizontal-Expand:"); 
	gtk_misc_set_alignment(GTK_MISC(label), 1,0.5);
	gtk_table_attach(GTK_TABLE(table), label, 0,1,7,8,GTK_FILL|GTK_SHRINK,GTK_FILL|GTK_SHRINK,0,0);	
	label = gtk_check_button_new();
	gtk_table_attach_defaults(GTK_TABLE(table), label, 1,2,7,8);	
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(label),(gdouble)cfg_get_single_value_as_int_with_default(config, "qosd-plugin","expand",0));
	g_signal_connect(G_OBJECT(label), "toggled", G_CALLBACK(qosd_x_expand_changed), NULL);


	label = gtk_button_new_with_label("Test Settings");
	g_signal_connect(G_OBJECT(label), "clicked", G_CALLBACK(qosd_test_settings), NULL);
	wid2 = gtk_alignment_new(1,0.5, 0,0);
	gtk_container_add(GTK_CONTAINER(wid2), label);
	gtk_table_attach(GTK_TABLE(table), wid2, 1,2,8,9,GTK_SHRINK|GTK_FILL,GTK_FILL|GTK_SHRINK,0,0);	


	gtk_container_add(GTK_CONTAINER(container), qosd_vbox);
	gtk_widget_show_all(container);
}

/* mpd changed */

void   qosd_mpd_status_changed(MpdObj *mi, ChangedStatusType what, void *data)
{
	if(what&(MPD_CST_SONGID|MPD_CST_STATE))
	{
        int state = mpd_player_get_state(mi);
        if(state&MPD_STATUS_STATE_PLAY)
            qosd_song_changed(mi);
		return;
	}
	/* don't do this on song change */
	if(what&MPD_CST_VOLUME)
	{
	}
	if(what&MPD_CST_REPEAT)
	{
	}
	if(what&MPD_CST_RANDOM)
	{
	}

}
