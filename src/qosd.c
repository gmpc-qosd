/* gmpc-qosd (GMPC plugin)
 * Copyright (C) 2006-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <gtk/gtk.h>
#include <gmpc/plugin.h>
#include "qosd.h"
static GtkWidget *gk_window = NULL;
static gchar *utf8;
static PangoLayout *layout = NULL;
int xpadding = 10, ypadding=10,id=0;
GdkPixbuf *qosd_cover_pb = NULL;
double value = 0;


static int destroy_window()
{
	gtk_widget_destroy(gk_window);
	if(id)g_source_remove(id);
	id= 0;
	gk_window = NULL;

	g_object_unref(layout);
    layout = NULL;
	return FALSE;
}

static int close_window()
{
    if(gk_window){
        destroy_window();
        gk_window = NULL;
    }
}
static void cc_draw_curved_rectangle(cairo_t *cc, int width, int height)
{
	cairo_rectangle(cc,0,0,width,height);
	cairo_fill_preserve(cc);
	cairo_set_source_rgba(cc,1,1,1,value);
	cairo_stroke(cc);
}
static int expose()
{
	int width=0,height=0;
	int text_width=0, text_height=0;
	int pb_width=0, pb_height=0;
	int xoffset = 0;
	cairo_t *cc = NULL;
    cairo_t *context;
    cairo_surface_t *surface;
	gtk_window_get_size(GTK_WINDOW(gk_window), &width, &height);
    context = gdk_cairo_create(gk_window->window);
    cairo_set_operator(context, CAIRO_OPERATOR_SOURCE);
    surface = cairo_surface_create_similar(cairo_get_target(context),
            CAIRO_CONTENT_COLOR_ALPHA,
            width,
            height);
    cc = cairo_create(surface);


	cairo_set_source_rgba(cc,
			(double)cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-rcolor",0)/65535.0,
			(double)cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-gcolor",0)/65535.0,
			(double)cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-bcolor",0)/65535.0,
			value*((double)cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "background-acolor", 50000)/65535.0));

    if(cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "expand", 0))
    {
		int pos = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "vposition", 0);
        cairo_rectangle(cc,0,0,width,height);
        cairo_fill(cc);
        cairo_set_source_rgba(cc,1,1,1,value);
        if(pos > 0)
        {
            cairo_move_to(cc,0,0);
            cairo_line_to(cc,width,0);
            cairo_stroke(cc);
        }
        if(pos < 2)
        {
            cairo_move_to(cc,0,height);
            cairo_line_to(cc,width,height);
            cairo_stroke(cc);
        }

    }else{
        cc_draw_curved_rectangle(cc,width,height);
    }

	if(qosd_cover_pb)
	{
		pb_width = gdk_pixbuf_get_width(qosd_cover_pb);;
		pb_height = gdk_pixbuf_get_height(qosd_cover_pb);
		if(height >= (pb_height+ypadding))
		{
			cairo_set_source_rgba(cc,1,1,1,value);
			cairo_rectangle(cc,xpadding/2+4,(height-pb_height)/2,pb_width,pb_height);
			cairo_stroke_preserve(cc);
			cairo_clip(cc);
			gdk_cairo_set_source_pixbuf (cc,qosd_cover_pb,xpadding/2+4,(height-pb_height)/2);
			cairo_paint_with_alpha(cc,value);
			cairo_reset_clip(cc);

			xoffset = 90;
		}
	}


	pango_layout_get_pixel_size(layout, &text_width, &text_height);


    int xpadding2= (width-text_width);
	cairo_set_source_rgba(cc,0,0,0,value);
	cairo_move_to(cc,
			xoffset+xpadding2/2+2,
			(height-text_height)/2 /*ypadding/2*/+2
		     );
	pango_cairo_show_layout(cc, layout);

	cairo_move_to(cc,
			xoffset+xpadding2/2,
			(height-text_height)/2/*-ypadding/2*/
		     );

	cairo_set_source_rgba(cc,
			(double)cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-rcolor", 65535)/65535.0,
			(double)cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-gcolor", 65535)/65535.0,
			(double)cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-bcolor", 65535)/65535.0,
			value*(cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "text-acolor", 65535)/65535.0));

	pango_cairo_show_layout(cc, layout);

	cairo_destroy(cc);
    cairo_set_source_surface(context, surface, 0, 0);
    cairo_paint(context);
    cairo_surface_destroy(surface);
    cairo_destroy(context);
}
static void reshow_window()
{
    gtk_widget_queue_draw(gk_window);
}
void qosd_show_popup(gchar *string)
{
	if(utf8) g_free(utf8);
	utf8 = g_strdup(string);

	if(gk_window)
	{
        destroy_window();
	}
	{
		GdkRectangle msize;
		int screen_w, screen_h;
		int width,height;
		int x,y,expand=0;
		PangoFontDescription *desc;
		cairo_text_extents_t extents;
		cairo_t *cc = NULL;
		GdkScreen *screen = NULL;
		GdkWindow *root =   NULL;
		GdkColormap *colormap;

		/* create the "popup" widow  */
		gk_window = gtk_window_new(GTK_WINDOW_POPUP);

        screen = gtk_window_get_screen(GTK_WINDOW(gk_window));
        colormap = gdk_screen_get_rgba_colormap(screen);

        if (colormap != NULL && gdk_screen_is_composited(screen))
        {
            gtk_widget_set_colormap(gk_window, colormap);
        }




		/* make the window a drawable */
		gtk_widget_set_app_paintable(gk_window, TRUE);
		/* set the expose signal */
		g_signal_connect(G_OBJECT(gk_window), "expose-event", G_CALLBACK(expose), NULL);

		/* always on top */
		gtk_window_set_keep_above(GTK_WINDOW(gk_window), TRUE);

		/* We need to get the root window of the screen the popup is located 
		 * We use this to get the screensize, and to make a screenshot
		 */
		gdk_screen_get_monitor_geometry(screen, 0,&msize);
		root = gdk_screen_get_root_window(screen);

		/* get the size of the monitor */
		screen_w = msize.width;
		screen_h = msize.height;

		/* to calculate the needed size of the popup we need to create a 
		 * cairo context, and get a pango layout from that.
		 * I cannot take the window as a drawable, because it needs to showed before it's
		 * valid 
		 * TODO: if I make the popup size fixed, I can remove this "hack"
		 */
		/* create cairo context */
		cc = gdk_cairo_create(root);
		/* create pango layout, I am gonna keep this one around to reuse... maybe not the best thing? */
		layout = pango_cairo_create_layout(cc);

		/* set the text */
		pango_layout_set_markup(layout, utf8,-1);
		/* start making a description, so I can configure font size and stuff */
		desc = pango_font_description_new();
		/* set font info */
		pango_font_description_set_weight(desc, PANGO_WEIGHT_BOLD);
		pango_font_description_set_size(desc, 16*PANGO_SCALE);
		pango_layout_set_font_description(layout,desc);
		/* clean up the description */
		pango_font_description_free(desc);
		/* set alignement */
		switch(cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "hposition", 0))
		{
			case 0:
				pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
				break;
			case 1:
				pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
				break;
			case 2:
				pango_layout_set_alignment(layout, PANGO_ALIGN_RIGHT);
				break;

			default:
				break;
		}


		/* get the size the  string needs */
		pango_layout_get_pixel_size(layout, &width, &height);
		/* if the window is going to be bigger then the actual screen, 
		 * set wrapping. (always setting wrapping will screw up the PANGO_ALIGN_CENTER
		 */
		if(width >= (screen_w-2*xpadding))
		{
			/* set wrapper */
			pango_layout_set_wrap(layout,PANGO_WRAP_CHAR);
			pango_layout_set_width(layout, PANGO_SCALE*(screen_w-2*xpadding));
			/* refetch size */
			pango_layout_get_pixel_size(layout, &width, &height);
		}
		if(qosd_cover_pb){
			height = (height > gdk_pixbuf_get_height(qosd_cover_pb))? height:gdk_pixbuf_get_height(qosd_cover_pb);
			width += 10+gdk_pixbuf_get_width(qosd_cover_pb);
		}
		/* calculate screen position */
		switch(cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "hposition", 0))
		{
			case 0:
				x = 0;
				x += msize.x+cfg_get_single_value_as_int_with_default(config, "qosd-plugin","y-offset",0);
				break;
			case 1:
				x = msize.x+screen_w/2-(width+xpadding)/2;

				break;
			case 2:
				x = msize.x+screen_w - (width+xpadding);
				x -= cfg_get_single_value_as_int_with_default(config, "qosd-plugin","y-offset",0);
				break;
			default:
				break;
		}
		switch(cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "vposition", 0))
		{
			case 0:                                                                                   		
				y = msize.y;
				y += cfg_get_single_value_as_int_with_default(config, "qosd-plugin","x-offset",0);
				break;
			case 1:
				y = msize.y+screen_h/2-(height+ypadding)/2;

				break;
			case 2:
				y = msize.y+screen_h - (height+ypadding);
				y -= cfg_get_single_value_as_int_with_default(config, "qosd-plugin","x-offset",0);
				break;
			default:
				break;
		}

		/* move the window in place */
        expand = cfg_get_single_value_as_int_with_default(config, "qosd-plugin", "expand", 0);
        if(expand)
        {
            gtk_window_move(GTK_WINDOW(gk_window), 0,y);
            gtk_window_resize(GTK_WINDOW(gk_window), screen_w, height+ypadding); 
        }else{
            gtk_window_move(GTK_WINDOW(gk_window), x,y);
            gtk_window_resize(GTK_WINDOW(gk_window), width+xpadding, height+ypadding); 
        }
        cairo_destroy(cc);

		/* show the widget */
		gtk_widget_show(gk_window);
		id = g_timeout_add(cfg_get_single_value_as_int_with_default(config, "qosd-plugin","timeout",3)*1000, 
				(GSourceFunc)(close_window), NULL);
		/* fade in effect */
		value = 1;
	}
}
